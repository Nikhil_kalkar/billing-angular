import { Injectable } from '@angular/core';
import { ENUM } from '../enum';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { LoginService } from '../login/login.service';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  token: any;
  constructor(private http: HttpClient, private toastr: ToastrService, public loginservice: LoginService) {
    this.token = 'Bearer ' + this.loginservice.getToken();
  }

  getproductlist(param) {
    const url = ENUM.domain + '' + ENUM.url.productlisturl;
    return new Promise(resolve => {
      // { headers: {'x-access-token': this.token}}
      this.http.post(url, param)
        // .map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve(err);
            if (err) {
              this.toastr.error('Please check server connection', 'Server Error!');
            }
          });
    });
  }

  addeditproduct(param) {
    const url = ENUM.domain + '' + ENUM.url.addeditproducturl;
    return new Promise(resolve => {
      this.http.post(url, param)
        // .map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve(err);
            if (err) {
              this.toastr.error('Please check server connection', 'Server Error!');
            }
          });
    });
  }

  product(param) {
    const url = ENUM.domain + '' + ENUM.url.productdetailurl;
    return new Promise(resolve => {
      this.http.post(url, param)
        // .map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve(err);
            if (err) {
              this.toastr.error('Please check server connection', 'Server Error!');
            }
          });
    });
  }
}

