import { TestBed } from '@angular/core/testing';

import { ChalanService } from './chalan.service';

describe('ChalanService', () => {
  let service: ChalanService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ChalanService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
