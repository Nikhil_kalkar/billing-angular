import { Injectable } from '@angular/core';
import { ENUM } from '../enum';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { LoginService } from '../login/login.service';

@Injectable({
  providedIn: 'root'
})
export class ChalanService {
  token: any;
  constructor(private http: HttpClient, private toastr: ToastrService, public loginservice: LoginService) {
    this.token = 'Bearer ' + this.loginservice.getToken();
  }

  getallchalandropdown(param) {
    const url = ENUM.domain + '' + ENUM.url.chalandropdownlisturl;
    return new Promise(resolve => {
      this.http.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve(err);
            if (err) {
              this.toastr.error('Please check server connection', 'Server Error!');
            }
          });
    });
  }

  getallchalan(param) {
    const url = ENUM.domain + '' + ENUM.url.chalanlisturl;
    return new Promise(resolve => {
      this.http.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve(err);
            if (err) {
              this.toastr.error('Please check server connection', 'Server Error!');
            }
          });
    });
  }
  
  chalan(param) {
    const url = ENUM.domain + '' + ENUM.url.chalandetailurl;
    return new Promise(resolve => {
      this.http.post(url, param)
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve(err);
            if (err) {
              this.toastr.error('Please check server connection', 'Server Error!');
            }
          });
    });
  }

}

