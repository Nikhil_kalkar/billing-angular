import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ENUM } from '../enum';
import { ToastrService } from 'ngx-toastr';
@Injectable({
  providedIn: 'root'
})
export class LoginService {
  token: any;
  constructor(private http: HttpClient, private toastr: ToastrService, ) {
    this.token = localStorage.getItem('token');
    // console.log(this.token);
  }

  authUserData(param) {
    // var url =ENUM.domain + '' + ENUM.url.auth;
    const url = ENUM.domain + '' + ENUM.url.auth;
    return new Promise(resolve => {
      this.http.post(url, param)
        // .map(res => res.json())
        .subscribe(data => {
          // console.log(data['token'])
          this.token = data['token'];
          localStorage.setItem('token', this.token);
          // let userdata = JSON.stringify(data);
          resolve(data);
        },
          err => {
            resolve(err);
            if (err) {
              this.toastr.error('Please check server connection', 'Server Error!');
            }
          });
    });
  }
  logout(): void {
    // clear token remove user from local storage to log user out
    this.token = null;
    localStorage.removeItem('currentUser');
  }
  getToken(): string {
    let token = null;
    try {
      if (localStorage.getItem('token')) {
        token = localStorage.getItem('token');
      }
    } catch (e) {
      console.error(e);
      return token;
    }
    return token;
  }
}
