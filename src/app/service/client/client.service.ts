import { Injectable } from '@angular/core';
import { ENUM } from '../enum';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { LoginService } from '../login/login.service';
@Injectable({
  providedIn: 'root'
})
export class ClientService {
  token:any;
  constructor(private http: HttpClient, private toastr: ToastrService, public loginservice: LoginService) {
    this.token = 'Bearer '+ this.loginservice.getToken();
   }

  getclientlist(param) {
    const url = ENUM.domain + '' + ENUM.url.clientlisturl;
    return new Promise(resolve => {
      // { headers: {'x-access-token': this.token}}
      this.http.post(url, param)
        // .map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve(err);
            if (err) {
              this.toastr.error('Please check server connection', 'Server Error!');
            }
          });
    });
  }

  addeditclient(param) {
    const url = ENUM.domain + '' + ENUM.url.addeditclienturl;
    return new Promise(resolve => {
      this.http.post(url, param)
        // .map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve(err);
            if (err) {
              this.toastr.error('Please check server connection', 'Server Error!');
            }
          });
    });
  }

  client(param){
    const url = ENUM.domain + '' + ENUM.url.clientdetailurl;
    return new Promise(resolve => {
      this.http.post(url, param)
        // .map(res => res.json())
        .subscribe(data => {
          resolve(data);
        },
          err => {
            resolve(err);
            if (err) {
              this.toastr.error('Please check server connection', 'Server Error!');
            }
          });
    });
  }
}
