export let ENUM = {
    domain: 'http://localhost:9010',

    url: {
        auth: '/api/client/userLogin',
        /*  ----------------Client------------*/
        clientlisturl: '/api/client/getallclientlist',
        addeditclienturl: '/api/client/addeditclientdetails',
        clientdetailurl: '/api/client/getallclientdetails',

        /* ------------ product ------- */
        productlisturl: '/api/client/getallproductlist',
        addeditproducturl : '/api/client/addeditproductdetails',
        productdetailurl: '/api/client/getallproductdetails',

        /* ------------ Chalan ------- */
        chalandropdownlisturl: '/api/client/getallchalandropdown',
        chalanlisturl: '/api/client/getallchalanlist',
        chalandetailurl: '/api/client/getallchalanlist',
    }
}