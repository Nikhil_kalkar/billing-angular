import { Component, OnInit } from '@angular/core';
import { ClientService } from '../../service/client/client.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { FormBuilder,FormGroup,Validators,FormControl} from "@angular/forms";
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css']
})
export class ClientComponent implements OnInit {
  userdetails: any = [];
  companydeatail: any = [];
  list: boolean = true;
  clients:any;
  addclient: boolean = false;
  public addclientform: FormGroup;
  title:any;
  cdetail:any = {
    client_id: 0,
    client_name: '',
    contact_person: '',
    client_address: '',
    client_phone: '',
    client_email: '',
    client_gst_no: '',
    client_pan: '',
    state: '',
    state_code: '',
  };
  
  constructor(public clientservice : ClientService, private spinner: NgxSpinnerService,
    public formBuilder: FormBuilder, public toastr: ToastrService, public  router: Router ) {
    
   }

  ngOnInit() {
    this.userdetails = JSON.parse(localStorage.getItem('userdata'));
    this.companydeatail = this.userdetails[2][0];
    this.getclints();
  }
  
  getclints(){
    this.spinner.show();
    const param ={
      company_id :  this.companydeatail.company_id,
    }
    this.clientservice.getclientlist(param).then(res=>{
      if(res['type']== true){
        if(res['data'].length){
          this.clients = res['data'];
          this.list =  true;
        } else {
          this.list = false;
        }
      }
      this.spinner.hide();
    },err=>{
      this.toastr.error("something went wrong please try again");
      this.spinner.hide();
      console.log(err);
    })
  }
  creataddeditclientform(data){
    this.addclientform = this.formBuilder.group({
      cId: [data.client_id],
      cname: [data.client_name, Validators.required],
      cpname: [data.contact_person ,Validators.required],
      address: [data.client_address, Validators.required],
      phone: [data.client_phone, Validators.required],
      email: [data.client_email, Validators.required],
      gst: [data.client_gst_no, Validators.required],
      pan: [data.client_pan, Validators.required],
      state: [data.state, Validators.required],
      scod: [data.state_code, Validators.required],
    });		
  }
  clientadd(clientID){
    if(clientID){
      this.clientdetail(clientID);
      this.title = 'Edit';
      // this.addclientform.value.cId = clientID;
    } else{
      this.title = 'Add'
      // this.addclientform.value.cId = clientID = 0;
    }
      this.addclient = true;
      this.creataddeditclientform(this.cdetail);
  }

  back(){
    this.addclient = false;
    this.addclientform.reset();
  }
  home(){
    this.router.navigateByUrl('/menus');
  }
  clientdetail(cId){
    this.spinner.show();
    const param = {
      company_id : this.companydeatail.company_id,
      clientId: cId
    }
    this.clientservice.client(param).then(res=>{
      if(res['type']== true){
        this.cdetail = res['data'][0];
        this.creataddeditclientform(this.cdetail);
      }
      this.spinner.hide();
    }, err=>{
      this.spinner.hide();
      this.toastr.error("something went wrong please try again");
      console.log(err);
    })
  }

  onsubmitclient(){
    if (this.addclientform.valid) {
      this.spinner.show();
      // this.addclientform.value.cId = 0;
      this.addclientform.value.company_id = this.companydeatail.company_id;
      this.clientservice.addeditclient(this.addclientform.value).then(res=>{
        if(res['type']== true){
          this.toastr.success("You have successfully added the client");
          this.addclient = false;
          this.getclints();
        } else{
          this.toastr.error("something went wrong please try again");
        }
      }, err=>{
        this.toastr.error("something went wrong please try again");
      })
      this.spinner.hide();
    } else{
      this.validateAllFormFields(this.addclientform);
    }
    
  }
  
  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }
}


