import { Component, OnInit, ViewEncapsulation} from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menus',
  templateUrl: './menus.component.html',
  styleUrls: ['./menus.component.css'],
  encapsulation:ViewEncapsulation.None
})
export class MenusComponent implements OnInit {
  userdetails: any = [];
  menus: any = [];
  companydeatail: any = [];
  constructor(public  router: Router,) { }

  ngOnInit(): void {
    this.userdetails = JSON.parse(localStorage.getItem('userdata'));
    
    this.menus = this.userdetails[1];
    this.companydeatail = this.userdetails[2][0];
  }
  gotonext(menu){
    this.router.navigateByUrl(menu);
  }
}
