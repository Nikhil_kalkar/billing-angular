import { Component } from '@angular/core';
import { FormBuilder,FormGroup,Validators,FormControl} from "@angular/forms";
import { Router } from '@angular/router';
import { AuthenticationService } from '../../service/auth/authentication.service';
import { LoginService } from '../../service/login/login.service';
@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})
export class LoginComponent {

  public contactform: FormGroup;
  errmsg:boolean = false;
	constructor(public  router: Router,public formBuilder: FormBuilder, public asp: AuthenticationService,
		public loginservice: LoginService) { 
		this.createloginform();
	}

	createloginform(){
	  this.contactform = this.formBuilder.group({
        username: ["RSAdmin", Validators.required],
        password: ["admin", Validators.required]
      });		
	}

	login(data){
		this.errmsg = false;
		if (this.contactform.valid) {
			this.loginservice.authUserData(this.contactform.value).then(res=>{
				if(res['type'] == true){
					localStorage.setItem('userdata', JSON.stringify(res['data']));
					this.router.navigateByUrl('/menus');
				} else {
					this.errmsg = true;
				}
			}, err=>{
				 console.log(err);
			})
	    } else {
	      this.validateAllFormFields(this.contactform);
	    }
	}


  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }
}
