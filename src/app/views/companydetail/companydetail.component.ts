import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-companydetail',
  templateUrl: './companydetail.component.html',
  styleUrls: ['./companydetail.component.css']
})
export class CompanydetailComponent implements OnInit {
  userdetails: any = [];
  // menus: any = [];
  companydeatail: any = [];
  constructor() { }

  ngOnInit(): void {
    this.userdetails = JSON.parse(localStorage.getItem('userdata'));
    
    // this.menus = this.userdetails[1];
    this.companydeatail = this.userdetails[2][0];
  }

}
