import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from "@angular/forms";
import { ToastrService } from 'ngx-toastr';
import { ChalanService } from '../../service/chalan/chalan.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-chalan',
  templateUrl: './chalan.component.html',
  styleUrls: ['./chalan.component.css']
})
export class ChalanComponent implements OnInit {
  userdetails: any = [];
  companydeatail: any = [];
  columns:string[];
  products:any;
  clients:any;
  chalan:any;
  Dropdown:any;
  list: boolean = true;
  addchalan: boolean = false;
  cdetail: any = {
    chalan_id : 0,
    client: '',
    tableRowArray:[{name:'',address:'',salary:'',isActive:true}]
  }
  title:any;
  public addchalanform: FormGroup;
  constructor(public chalanservice: ChalanService, private spinner: NgxSpinnerService,
    public formBuilder: FormBuilder, public toastr: ToastrService, public  router: Router) { }

  ngOnInit(): void {
    this.userdetails = JSON.parse(localStorage.getItem('userdata'));
    this.companydeatail = this.userdetails[2][0];
    this.getchalanlist();
    this.columns = ["Name", "Address", "Salary", "IsActive", "Delete"];  
    this.creataddeditproductform(this.cdetail);
  }
  
  getchalandropdown() {
    this.spinner.show();
    const param = {
      company_id: this.companydeatail.company_id,
    }
    console.log(param);
    this.chalanservice.getallchalandropdown(param).then(res => {
      if (res['type'] == true) {
          this.Dropdown = res;
          console.log(this.Dropdown);
          if(this.Dropdown['client'])
          {
            this.clients = this.Dropdown['client'];
          }
          if(this.Dropdown['product'])
          {
            this.products = this.Dropdown['product'];
          }
      }
      this.spinner.hide();
    }, err => {
      this.toastr.error("something went wrong please try again");
      this.spinner.hide();
      console.log(err);
    })
  }
  
  getchalanlist() {
    this.spinner.show();
    const param = {
      company_id: this.companydeatail.company_id,
    }
    this.chalanservice.getallchalan(param).then(res => {
      console.log(res);
      if (res['type'] == true) {
        if (res['data'].length) {
          this.chalan = res['data'];
          this.list = true;
        } else {
          this.list = false;
        }
      }
      this.spinner.hide();
    }, err => {
      this.toastr.error("something went wrong please try again");
      this.spinner.hide();
      console.log(err);
    })
  }

  back(){
    this.addchalan = false;
    this.addchalanform.reset();
  }

  home(){
    this.router.navigateByUrl('/menus');
  }

  chalanadd(chalanId){
    console.log(chalanId);
    if(chalanId){
      this.chalandetail(chalanId);
      this.title = 'Edit';
    } else{
      this.title = 'Add'
    }
    this.addchalan = true;
    this.getchalandropdown();
    this.creataddeditproductform(this.cdetail);
  }

  creataddeditproductform(data){
    console.log(data);
    this.addchalanform = this.formBuilder.group({
      chalanid: [data.product_id],
      client: [data.client, Validators.required],
      tableRowArray: this.formBuilder.array([  
        this.createTableRow()  
    ])
    });		
  }
  
  private createTableRow(): FormGroup {  
    return this.formBuilder.group({  
        name: new FormControl(null, {  
            validators: [Validators.required, Validators.minLength(3), Validators.maxLength(50)]  
        }),  
        address: new FormControl(null, {  
            validators: [Validators.required, Validators.maxLength(500)]  
        }),  
        salary: new FormControl(null, {  
            validators: [Validators.required, Validators.pattern(/^\d{1,6}(?:\.\d{0,2})?$/), Validators.minLength(3), Validators.maxLength(50)]  
        }),  
        isActive: new FormControl({  
            value: true,  
            disabled: true  
        })  
    });  
} 

get tableRowArray(): FormArray {  
  return this.addchalanform.get('tableRowArray') as FormArray;  
} 

addNewRow(): void {  
  this.tableRowArray.push(this.createTableRow());  
}  

onDeleteRow(rowIndex:number): void {  
  this.tableRowArray.removeAt(rowIndex);  
}

  clientchange(){
    console.log(this.cdetail,this.addchalanform.value)
  }
  chalandetail(pId){
    this.spinner.show();
    const param = {
      company_id : this.companydeatail.company_id,
      productId: pId
    }
    this.chalanservice.chalan(param).then(res=>{
      console.log(res);
      if(res['type']== true){
        this.cdetail =res['data'][0];
        console.log(this.cdetail);
        this.creataddeditproductform(this.cdetail);
      }
      this.spinner.hide();
    }, err=>{
      this.spinner.hide();
      this.toastr.error("something went wrong please try again");
      console.log(err);
    })
  }

  submitchalan(){
    if (this.addchalanform.valid) {
      console.log(this.addchalanform.value)
    } else{
      this.validateAllFormFields(this.addchalanform);
    }
    
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }
}
