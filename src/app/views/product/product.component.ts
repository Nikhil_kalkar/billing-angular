import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { FormBuilder, FormGroup, Validators, FormControl } from "@angular/forms";
import { ToastrService } from 'ngx-toastr';
import { ProductService } from '../../service/product/product.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  userdetails: any = [];
  companydeatail: any = [];
  products:any;
  list: boolean = true;
  addproduct: boolean = false;
  pdetail: any = {
    product_id : 0,
    product_name: '',
    product_description: '',
  }
  title:any;
  public addproductform: FormGroup;
  constructor(public productservice: ProductService, private spinner: NgxSpinnerService,
    public formBuilder: FormBuilder, public toastr: ToastrService, public  router: Router) { }

  ngOnInit(): void {
    this.userdetails = JSON.parse(localStorage.getItem('userdata'));
    this.companydeatail = this.userdetails[2][0];
    this.getproducts();
  }

  getproducts() {
    this.spinner.show();
    const param = {
      company_id: this.companydeatail.company_id,
    }
    this.productservice.getproductlist(param).then(res => {
      if (res['type'] == true) {
        if (res['data'].length) {
          this.products = res['data'];
          this.list = true;
        } else {
          this.list = false;
        }
      }
      this.spinner.hide();
    }, err => {
      this.toastr.error("something went wrong please try again");
      this.spinner.hide();
      console.log(err);
    })
  }
  productadd(productId){
    if(productId){
      this.productdetail(productId);
      this.title = 'Edit';
    } else{
      this.title = 'Add'
    }
      this.addproduct = true;
      this.creataddeditproductform(this.pdetail);
  }

  productdetail(pId){
    this.spinner.show();
    const param = {
      company_id : this.companydeatail.company_id,
      productId: pId
    }
    this.productservice.product(param).then(res=>{
      if(res['type']== true){
        this.pdetail =res['data'][0];
        this.creataddeditproductform(this.pdetail);
      }
      this.spinner.hide();
    }, err=>{
      this.spinner.hide();
      this.toastr.error("something went wrong please try again");
      console.log(err);
    })
  }

  creataddeditproductform(data){
    this.addproductform = this.formBuilder.group({
      proid: [data.product_id],
      proname: [data.product_name, Validators.required],
      prodesc: [data.product_description,Validators.required],
    });		
  }

  back(){
    this.addproduct = false;
    this.addproductform.reset();
  }
  home(){
    this.router.navigateByUrl('/menus');
  }
  onsubmitproduct(){
    if (this.addproductform.valid) {
      this.spinner.show();
      // this.addclientform.value.cId = 0;
      this.addproductform.value.company_id = this.companydeatail.company_id;
      this.productservice.addeditproduct(this.addproductform.value).then(res=>{
        if(res['type']== true){
          this.toastr.success("You have successfully added the client");
          this.addproduct = false;
          this.getproducts();
        } else{
          this.toastr.error("something went wrong please try again");
        }
      }, err=>{
        this.toastr.error("something went wrong please try again");
      })
      this.spinner.hide();
    } else{
      this.validateAllFormFields(this.addproductform);
    }
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }
}
