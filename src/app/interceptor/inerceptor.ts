import { Injectable } from '@angular/core';
// import { ErrorDialogService } from '../error-dailog/errordialog.service';
// import { ToasterModule, ToasterService, Toast } from 'angular2-toaster';
import { ToastrService } from 'ngx-toastr';
import {
    HttpInterceptor,
    HttpRequest,
    HttpResponse,
    HttpHandler,
    HttpEvent,
    HttpErrorResponse
} from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable()
export class HttpConfigInterceptor implements HttpInterceptor {
    constructor(public toastr: ToastrService) { }
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const token: string = localStorage.getItem('token');

        if (token) {
            request = request.clone({ headers: request.headers.set('Authorization', 'Bearer ' + token) });
        }

        if (!request.headers.has('Content-Type')) {
            request = request.clone({ headers: request.headers.set('Content-Type', 'application/json') });
        }

        request = request.clone({ headers: request.headers.set('Accept', 'application/json') });
        return next.handle(request).pipe(
            map((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {
                }
                return event;
            }),
            catchError((err: HttpErrorResponse) => {
                console.log('err',err);
                let data = {};
                data = {
                    reason: err && err.error.reason ? err.error.reason : '',
                    status: err.status
                };
                if (err instanceof HttpErrorResponse) {
                    if (err.status === 401) {
                        console.log(err);
                        this.toastr.error('Please Login First');
                        localStorage.clear();
                        window.open('#/login','_self');
                    }
                }
                return throwError(err);
            }));
    }
}
